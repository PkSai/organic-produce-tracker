# Asset Tracking System

![image](https://gitlab.com/PkSai/organic-produce-tracker/-/raw/main/oog1.png)


A Hyperledger Fabric based Organic Produce tracking system. Tracks various produce when it leaves the farm till the point where it reaches the retail outlet.

# Features

![image](https://gitlab.com/PkSai/organic-produce-tracker/-/raw/main/oog2.png)

- **BatchID**
- **Type**
- **Production date**
- **Verification**

**Built with Gin and chaincode written in Go**

